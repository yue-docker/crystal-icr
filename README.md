debian-crystal
==============

This is a Debian Jessie docker image with crystal and icr installed.

Prerequisites
-------------

- Docker

Usage
-----

```text
docker pull yueyehua/debian-crystal-icr
docker run \
  -d \                                           # daemonize
  --privileged \                                 # for systemd
  -v /sys/fs/cgroup:/sys/fs/cgroup:ro \          # for systemd
  --name crystal \                               # container name
  -h crystal \                                   # hostname
  yueyehua/debian-crystal-icr
docker exec -ti crystal bash
[Do something here]
docker stop crystal
docker rm crystal
```
